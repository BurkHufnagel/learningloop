import { createQuestion, voteOnQuestion, streamResults } from './'
import fetch from 'isomorphic-fetch'
import EventSource from 'eventsource'

describe('should interact with the question resources', () => {
    it(' should send a request to create a question', async () => {
        fetch.mockResolvedValue({ json: jest.fn() })
        await createQuestion({ question: 'A question' })
        expect(fetch).toBeCalledWith('https://server.learningloop.net/v1/questions', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ question: 'A question' }),
        });
    })

    it('should submit a vote on the provided question', async () => {
        fetch.mockResolvedValue({ json: jest.fn() })
        await voteOnQuestion({ id: 'random', level: 4 })
        expect(fetch).toBeCalledWith('https://server.learningloop.net/v1/questions/random/vote', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ level: 4 }),
        })
    })

    it('should start a stream of the results', () => {
        let res = streamResults()
        expect(res).toBeInstanceOf(EventSource)
    })
})
