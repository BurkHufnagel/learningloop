import fetch from 'isomorphic-fetch'
import EventSource from 'eventsource'

const BASEURL = (process.env.NODE_ENV === 'development') ? 'http://localhost:5000' : 'https://server.learningloop.net'

//const BASEURL = 'http://localhost:5000'

const createQuestion = async (question) => {
    return await fetch(`${BASEURL}/v1/questions`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(question)
    })
        .then(res => res.json())

}

const voteOnQuestion = async ({ id, level }) => {
    return await fetch(`${BASEURL}/v1/questions/${id}/vote`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ level })
    })
}

const streamResults = (id) => new EventSource(`${BASEURL}/v1/questions/${id}/stream`)
export { createQuestion, voteOnQuestion, streamResults }