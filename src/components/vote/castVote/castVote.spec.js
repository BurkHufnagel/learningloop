import React from 'react';
import { shallow } from 'enzyme';
import CastVoteComponent from '.';
import VoteButton from '../../voteButton'

describe('when A voter opens the voting selection screen', () => {
    let wrapper, props
    beforeEach(() => {

        props = {
            match: {
                params: {
                    voteName: 'random'
                }
            },
            options: ["5", "4,", "3", "2", "1", "0"],
            submitVote: jest.fn().mockResolvedValue({}),
            features: {
                submit_button_removal: true
            }
        }
        wrapper = shallow(<CastVoteComponent {...props} />);
    })

    it('should render without crashing', () => {
        expect(wrapper.find('#vote-page')).toHaveLength(1)
    })

    it('should display voting buttons based on number for result', () => {
        expect(wrapper.find(VoteButton)).toHaveLength(6)
    })

    it('should display submit button', () => {
        wrapper.setProps({ features: { submit_button_removal: false } })
        expect(wrapper.find('#submit-vote-btn').props().className).toContain('disabled')
        expect(wrapper.find('#submit-vote-btn')).toHaveLength(1)
        expect(wrapper.find('#submit-vote-btn').text()).toEqual('Submit Vote')
    })

    it('should not display submit button', () => {
        wrapper.setProps({ features: { submit_button_removal: true } })
        expect(wrapper.find('#submit-vote-btn')).toHaveLength(0)
    })

    it('should load with not vote selected', () => {
        let vote = wrapper.state().selectedVote
        expect(vote).toBeNull

    })
    it('should update the vote button visiually when it is selected', () => {
        expect(wrapper.find(VoteButton).first().prop('active')).toEqual('')
        wrapper.find(VoteButton).first().prop('handleChange')({ target: { value: '5' } })
        expect(wrapper.state().selectedVote).toEqual('5')
        expect(wrapper.find(VoteButton).first().prop('active')).toEqual('active')
    })

    it('should submit a vote request to the server after vote has been selected', () => {
        wrapper.setProps({ features: { submit_button_removal: false } })
        wrapper.find(VoteButton).first().prop('handleChange')({ target: { value: '5' } })
        expect(wrapper.find('#submit-vote-btn').props().className).not.toContain('disabled')

        wrapper.find('#submit-vote-btn').simulate('click')
        expect(props.submitVote).toBeCalled()
    })

    it('should only send vote request when an vote has been selected', () => {
        wrapper.setProps({ features: { submit_button_removal: false } })
        wrapper.find('#submit-vote-btn').simulate('click')
        expect(wrapper.find('#submit-vote-btn')).toHaveLength(1)

        expect(props.submitVote).not.toBeCalled()
    })

    it('should send vote request when an vote has been selected', async () => {
        wrapper.setProps({ features: { submit_button_removal: true } })
        await wrapper.find(VoteButton).first().prop('handleChange')({ target: { value: '5' } })
        expect(props.submitVote).toBeCalled()
    })
})