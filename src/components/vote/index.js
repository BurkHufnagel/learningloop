import React, { Component } from 'react';
import CastVoteComponent from './castVote'
import VoteResultsComponent from './results'
import LoadingComponent from '../shared/loadingComponent'
import VerticalLayoutComponent from '../shared/verticalLayout'

class VoteComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            question: null,
            selectedVote: '',
            showResults: false,
            loading: true,
            voteName: (props.match.params.voteName).toUpperCase()
        }
        let stream = props.service.streamResults(this.state.voteName)
        stream.onmessage = (e) => {
            this.setState({ loading: false, question: JSON.parse(e.data) })
        }
    }

    submitVote = async (vote) => {
        let { timeToVote, ...level } = vote
        window.gtag('event', 'user_voted', {
            'name': 'vote',
            event_label: this.state.voteName,
            'event_category': 'User Vote',
            'value': timeToVote
        });
        await this.props.service.voteOnQuestion({ id: this.state.voteName, ...level })
        this.setState({ showResults: true })
    }

    back = () => {
        this.setState({ showResults: false })
    }

    render() {
        return (
            <VerticalLayoutComponent classes=" align-items-center w-75">
                <LoadingComponent loading={this.state.loading}>
                    <h6 id="question-text" className="text-center mb-5" >{this.state.question ? this.state.question.text : ''}</h6>
                    {
                        this.state.question ? this.state.showResults ?
                            <VoteResultsComponent results={Object.entries(this.state.question.results).reverse()} features={this.props.features} back={this.back} /> :
                            <CastVoteComponent submitVote={this.submitVote} features={this.props.features} options={Object.keys(this.state.question.results).reverse()} />
                            :
                            <div className="spinner-border" role="status">
                                <span className="sr-only">Loading...</span>
                            </div>
                    }
                </LoadingComponent>
            </VerticalLayoutComponent>
        )


    }
}

export default VoteComponent;
