import React from 'react';
import { shallow } from 'enzyme';
import VoteResultsComponent from '.';
import VoteResults from '../../voteResults';

describe('when A voter opens the voting selection screen', () => {
    let wrapper, props
    beforeEach(() => {
        global.window.usabilla_live = jest.fn()
        props = {
            results: Object.keys({ "0": 0, "1": 0, "2": 0, "3": 0, "4": 0, "5": 3 }).reverse(),
            back: jest.fn(),
            features: {
                back_button_removal: false
            }
        }
        wrapper = shallow(<VoteResultsComponent {...props} />);
    })

    it('should render without crashing', () => {
        expect(wrapper.find('#results-page')).toHaveLength(1)
    })

    it('should display back button', () => {
        expect(wrapper.find('#back-vote-btn')).toHaveLength(1)
        expect(wrapper.find('#back-vote-btn').text()).toEqual('Back')
    })

    it('should not display back button if feature is set to no', () => {
        wrapper.setProps({ features: { back_button_removal: true } })
        expect(wrapper.find('#back-vote-btn')).toHaveLength(0)
    })

    it('should display results', () => {
        expect(wrapper.find(VoteResults)).toHaveLength(6)

    })

    it('should take user back to vote if back button clicked on result page', () => {
        wrapper.find('#back-vote-btn').simulate('click')
        expect(props.back).toBeCalled()
    })
})