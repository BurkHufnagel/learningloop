import React, { Component } from 'react';
import VoteResults from '../../voteResults';
import VerticalLayoutComponent from '../../shared/verticalLayout'


class VoteResultsComponent extends Component {
    constructor(props) {
        super(props)
        if (this.props.features.back_button_removal) {
            window.usabilla_live('trigger', 'removed_back_button_survery')
        } else if (this.props.features.submit_button_removal) {
            window.usabilla_live('trigger', 'remove_submit_button')
        } else {
            window.usabilla_live('trigger', 'results_survey')
        }

    }
    render() {

        return (
            <VerticalLayoutComponent id="results-page" classes=" align-items-center w-75">
                {this.props.results.map(([vote, results]) => {
                    return (<VoteResults key={vote} {...{ option: vote, results: results }} />)
                })}
                {this.props.features.back_button_removal ? '' : <button id="back-vote-btn" className="btn btn-primary btn-block mt-5" type="button" onClick={this.props.back}>Back</button>}
            </VerticalLayoutComponent>
        )
    }
}

export default VoteResultsComponent;
