/* eslint-disable no-useless-constructor */
import React from 'react'
const votesAndMeaning = [
    { vote: 'fist', text: 'I am not at all in alignment on this matter as it now stands.' },
    { vote: 'one', text: 'I have major concerns on this matter as it now stands.' },
    { vote: 'two', text: 'I have minor concerns on this matter as it now stands that I would like to discuss and attempt to resolve now.' },
    { vote: 'three', text: 'I have minor concerns that can wait until later to resolve.' },
    { vote: 'four', text: 'I am in alignment and will contribute to the success of this endeavor.' },
    { vote: 'five', text: 'I am wholeheartedly in support this endeavor and would like to play a lead role in its fulfillment.' },
]
const f1fLinks = [
    { text: 'TechTarget defines fist to five', link: 'https://whatis.techtarget.com/definition/fist-to-five-fist-of-five', image: "https://www.tetherview.com/wp-content/uploads/2018/11/techtarget-logo.png" },
    { text: "Lucid has a graphic on what first to five values mean", link: 'https://www.lucidmeetings.com/glossary/fist-five', image: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXgAAACGCAMAAADgrGFJAAAAeFBMVEX///+LxT+FwjCHwzXq9N7v9+aTyU263JSEwi3s9eHo89qt1n2IxDnj8NPf7s2Wy1L4/POm0nD9/vrz+eu32o3O5rPT6Lrc7ci02YiezmKQyEb2+vCw14LF4aTA35yp03agz2XR57fX6sCazVvJ46uBwR/D4KB8vhK7yxqDAAAM/0lEQVR4nO2d65aiOhCFhbQaNSqgtjZeW9Tz/m94BG+o7KoK0oO42L9mTUsuHyGpVJJKo4MUND5BbeVkq1VYFoGHRD7mAu2/CitZmfoH4AcaZOE2qcfAQ46uwQtVMHi3Bi9UDT5LNfiSVIMvSTX4klSDL0k1+JJUgy9JNfiSVIMvSTX4klSDL0k1+JJUgy9JNfiSVIMvSTX4klSDL0k1+JJUgy9JYw0W88PCsqjBZ2nUBSquejX4klSDL0k1+JJUgy9JNfiSVIMvSTX4klSDL0kfDd7rDL53q/b4qPbqMNn2m/Tm8+LUHExWs57v++tFezcYZfziU8HPB8t16MYzf3XR8d9ahf5qS9bsZQXdpe8cM1bmpGO+Oux9P8L/Q/CjJpDk2EjQeeHh5s53XNdklvAIQg1nW7rpd/pIXNbdcSsrZ+Maf3r3wz8Ev97rbP3X5Yp/LBd6mD1zEkw2GkC/cdBqvSXe4Bjl7tB5/0Ya+TWPeQ5/0xX8O/A9VHstAN93UQ4d8rlRW3HUzxxc04ZVzOcWnoR01kf0g+uPPwu81zawxT1LqQWoZB7w/SEq8k3G7V36uI8CP3H4ut9Jue3MDscefDDTog9NOedh4oPAd35QXQgps81Iyhp8NxR/aXqXPPE54H+NqMk9l6b33Ohtwf8qi7z16qPAj3M095NU6ylJS/Aru7x1+3PABxvL3j0towYPydmBt37n+vAp4L3IwpjJkHqYGFmBz/Gt6e1ngPeifN17drktwVv2MyepUf8DwAcvc3dMOM8J/jfX2GI2A1jB6oD3X+tnEik/H/h+zrzNBtGpDvj2C+NqqlC7POC9MO/HBp+rDPjv3HbkQ8Ipx60YfBEf22M5KgK++XL/fpbp2YPP18HTqgr41wfWa7FuNqUQ/KiwvFOqCPhdcW3ObGzB94rvaKoCXtTmjFFK4si5FV4GHpbxJVUD/Ixrc0qr4Xo2Hi82Lc05btXMDvwP/zJPS67adeUevEqA/6LbnNHDVf/qe5xPZ4y/Xl1WKUTgp1wvZ1wVLQ7f0+l0u5tFwpWxaoCHWZx+Gk0fUvWWpP/W/bYBD6dAJxk3+k0zHE0iEfoqgG+SbU4vM9IdUW5Mc5m+SsBDZ8s5983zjoRBJDAFqgB+TPTwxgG5zCjyczn4NdV8jZpkZj7h+/oKgPeo8odwiZCYbbpbMfgRNaybEFWx0+LIVwD8hGi7xJa2oAWfutg1AvBLcpzG8IIfxhCrAHhi0nodJrME1yCuYAXgqRmzoioYMFbo+4Pv4KZjfCrtho9dgydPGQ++QwyTOrt/v8gL8aNOFcDv8Meu6H2pXUjNPRmgPHiip2He+jF7spd6f/D4k1VtJnXYT7gnE5QHTxjxmt2NvKLIvz14D3/sOmtDelpwWDaL5O8s+DnmrsZs5YIhfLoC4LcQvFlzqUPfmomSv7PgCXeB4d46WfYKgIdwLh01JdxNBWTaF/A485unjRLR5N8ePGGV8WcX4Jz31Eux4HHmksqRU5B3B+/BqvM9zbHie5WtfVI2DnyAM//hM2+Qk+53B0+YhL900rECLwBK/syBx+5oN8szlyHsV3138L+w7po+MyIRB377cuY4hXcHj4e38PUrkTjwKzxpFuYwx6bBm4OH0/7UmnVuceBh1c7zAIHg8Pzu4KFBZkT2HC0OPKZGu2lSgnbVm4MPUAkcd8ckLRAHHrq5XPYg7EVwjHpz8Hgdws061GQpBrwHR0bJtPUkaJW9OXhs0MlmMLQY8HittyUe2OcVBY83E/HOQV4MeNhahdOnRBXt4wlLes4kLRADHi5hSSbNF6EFyDcHj+dPbgE3mzLg4Vu3saiQZfTm4PHyk5LXHYoBD986uwKTEpqIVBa8dO5IiQEPM1creR4Iz5uDx2ueRYT/ZcDDzKUuslizaoI/vOwtocSAh5nbgB9XE/zrbipKXItHf1afDx63+H/R1UDwFn18RbsaYl+LvO5QebsaG6vmTQdXfPD5DcxJ+NblXuG3NSdxjIUTeLxa/A8mUPCts5vIUnrTCdSUafHfZboMYOY2vhrkWi4ZPJyVn8HDF1PEkisHHmcu904Gb+qPh10J750Ur0VgMeDxKRy5Px66lksGj+coJ/B4jza5NV6o3P54+QrUuwYKQlbuBTzeNWpjSyPlXoGSr7lit8PL4BcQvKBZ4HAu5y4cLnumg0HkVe41V7k9Cc+u/SX4x9hrFhlcwePdi5Gg2jDgdFO0aZXYHS/IPFaA18tfBo97C345eoR70TN4+FodJRjg1hrtnUyMUQ48PucpXXjE4/Pr4GHpBR0hNtgu4PHUVfO7tPH3YkQtnpi+CTeXEJvMXwaPPRr88EeU6wweH92THMmAH7rsYAI2ZqX72PCRz9fB44k1P/xFuFxn8HjzoeAKSmiMnovGgfeIM8qi+Rv+ogsAD1cmDTv8feGjKteK4YOmvDEN5/zqkPydPZiAMxd8bw3qwGcB4PE5IcO5U4ggBVfwcOwWnIbBttzJ4GLBE+swbOUa1Ob+IsDjgZuzJ+e4WKnDZ8TXylx+A/0kjjpRY8EToTskPnkq4srr4Ju4esznSJ0DvYKHe+COvznQ6bOdIAsej86Cw550xMbXweNTSszxMDIMjeSAMeer4s4XCw4Y42kE75QnDsk6RYAnjtHSljwZZUF2pJ60m/Ap00vxefBUXCzN2PLEyOoUAZ7MgLoLiD7qfwXfJOKPaMJFicOmXNcxePB4f77DmVXkgfpCwBO2CdEk23T4qJSdTA1RhAcUv9jrhygIm4KneHFChP91yYTHKgA83lh6/CEY+4MFE8wxBR4v/8VCdadOw1++QwH4Dpl55pUvp6S5YJUFgKesVcddZPU2/YgtlzA0lqOy6z4gPsNrY5DEJGN66mzDrclfqVEA+IAMAqXCJzL9NX+ZUnpKTveWepFh21D3G9zcmgVE4XPDyVPLGo0Fl0UVcbslE5nRbR26t3Cc/cNQEpcxDZ4w5WMp0753nIx2LeJVpWY+oriTdJOP0Y8HqZncaLtWkljERYAnDL6TlHZ+1ovxrOcPDb6e8D6HNEsq/mGSvhuNv7vN+Xze7G5XvqL75dtcXwSe7ElP2WsT9dqH5XI19of5I60G3u3bgU/dgacHoJPOt56KypTkkAZPT0WS5JV7vpjSZS7JSlvfstjCC0FTiQNKx3fKWlTwHvxX29nv92bRtQFfYHD3Ww53vQcdhNBGdy5TGfg/CR9/Dz7o6cVg5I0Gi/16bgG+OC63HO7AB2z8TKnuDH9h/PgCg9fflAY/D38uA/5XlPhB4FP34MlwpDnLdT9eMraFPNk7v5r0qgpufM1VkhT4VjLPPPfwvXi6D596CHDKxndHMmgN6nGFhwoVbJHd5i5RKfhXOhvkyUqBX0XxJVdhK2z14loPx3LwdLxrLBOhxbFH8MV0Ng87E8S34lBR3WiZKcjjBt6Lw0UFztKbN1f74//29yP5LfWcwQekv1Af8rSm+VVAd/a4MiO/gOuQk7zeouWKG/hJPN4HTuJA2sQz4Z+dHDwTzxUl04a7CJ4Xk/O3uoue/LgWV85xvqVsuQsYjeEG3l/ewK/jM+OTjRx8Liyxe1YOPt81hynpJ4edzSWLmxxfnBkGAvCtfgI+DrHWiW8ibXSVBXjGvZ5ZrJZnBZ7zJDPSz/4sG/BsXPIMxUMKCz5w4qoG4WY883Vi3oxcG/AN24KZVjxVsAH/Evnn9m55n6tnfYlvcn0sDz6J/h+0hpvNj0o2wzbtwHtDK/LuT+JZsgL/Qm+zzzqaaneDceDbkdeJW5bvaqL4d6eu5isZhvqhFfiG92NRMH3eFGMHvrG1ua78JmMyN1raXpZudYnxeW8nD348a9wPrquZHfhGsJaSv11qYgm+0WSXULIS22T7YG3BN77F792Ys+nKg+/q4Aq+F4MPB5bg44VGUcHc4fV5W/CxSW3Z6E3mhUW5wDc6kpuG4uK3LuXnwTeiXbz5KrF115vj223JZ65XNTc8FpUOBWAPvjESLGDdZHQP7r6xB99o7ATLHEbfTowIwHf3x8p2E69kd9oY7fsYvMaX0Ux/aCzKvVuqywH+WLy1KxzIlesTezDQirShNiKPFlzm7jA1Q/7SJlPpow1LdavtV9Ish0D49qWj+j2NVmGMVuP7R6d7N1N7/GpjdcaKb/bGdRfkAbhVC9SPPkDcWcAKxlV0dulV2M7Gz9Qm3fx2F6MrWO6ZfYmkgu1sqPXdVY/GKK2Hs2kBB+HPmi5Cqv5KK/+7uNzuNd9FmXkbV/m54mF2Nvv1Ybfy9z8vR3VsTpeLTRQmi35OONz0ltMCAhbeKegu/Va84na3pJhc8elE4ymzl/hFdXZ+eMo70fGfrhkuvnMf9O/sZuvFjv7OLRQE3nw+9/6q5R0zGPW/D8c3fNmvF0b+bLn9+lvol7ybg9/DrHfsN9az9m7bLSC6wln/Axca+RYXyMWcAAAAAElFTkSuQmCC" },
    { text: "Derek Huether take on 'What is Fist of Five?': Why and How", link: 'https://www.derekhuether.com/blog/2012/06/06/what-is-fist-of-five', image: "https://static1.squarespace.com/static/5a879a9529f18794a662cac2/5a89dd85ec212d1cb4aabba7/5a89dece8165f58a198e7a86/1519007738238/idea-pictofigo-hi-029.png?format=300w" },
    { text: 'DZone has an indepth introduction into what first to five is', link: 'https://dzone.com/articles/fist-five', image: "https://automatedinsights.com/wp-content/uploads/2018/05/dzone-logo.png" }
]
class InfoPage extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        return (<div style={{ fontSize: '.80rem' }} className="px-3">
            <h5 id="title" className="text-center" >What is a Fist to Five poll?</h5>
            <p className="text-justify">
                Fist to Five, also called Fist of Five, is a technique to poll a group of people and to facilitate achieving consensus.
                This can easily be done when everyone is co-located by raising a hand and putting up a fist or one to five fingers. When not co-located, polling has to be done with the help of technology.
                This application is meant to be a simple solution to this problem. It requires a web client that supports HTML 5 and an Internet connection, e.g. a modern smart phone.
            </p>
            <ul className="list-unstyled">
                {votesAndMeaning.map(({ vote, text }) => {
                    return (<li key={vote}><strong>{vote.toUpperCase()}</strong> - {text}</li>)
                })}
            </ul>
            {this.props.features.pretty_info_links ?
                <div>
                    <h5 id="title" className="text-center" >For more on Fist to Five see </h5>
                    <ul className="list-unstyled">
                        {f1fLinks.map(({ text, link, image }, index) => <li key={index}>
                            <div>
                                <a className="" target='_blank' rel="noopener noreferrer" href={link}>
                                    <div>
                                        <img src={image} width="40px" alt={text}></img>
                                    </div>
                                    <p className="blackText">{text}</p>
                                </a>
                            </div>
                        </li>)}
                    </ul>
                </div>
                :
                <div>
                    <p>For more on Fist to Five see </p>
                    <ul className="list-unstyled">
                        {f1fLinks.map(({ link }, index) => <li key={index}><a href={link}>{link}</a></li>)}
                    </ul>
                </div>
            }

        </div >)
    }
}
export default InfoPage