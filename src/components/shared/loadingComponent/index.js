import React from 'react'

class LoadingCoomponent extends React.Component {
    render() {
        let spinner = (<div id="loading-spinner" className="spinner-border" role="status">
            <span className="sr-only">Loading...</span>
        </div>)


        return this.props.loading ? spinner : this.props.children
    }
}

export default LoadingCoomponent