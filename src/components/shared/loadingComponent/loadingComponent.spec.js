import React from 'react';
import { shallow } from 'enzyme';
import LoadingComponent from '.';


describe('', () => {
    let wrapper, props, RandomComponent = () => null


    it('renders without crashing', () => {
        props = {
            loading: true
        }
        wrapper = shallow(<LoadingComponent {...props} ><RandomComponent /></LoadingComponent>);
        expect(wrapper.find('#loading-spinner')).toHaveLength(1)
        expect(wrapper.find(RandomComponent)).toHaveLength(0)
    })

    it('should show children once loading is false', () => {
        props = {
            loading: false
        }
        wrapper = shallow(<LoadingComponent {...props} ><RandomComponent /></LoadingComponent>);

        expect(wrapper.find('#loading-spinner')).toHaveLength(0)
        expect(wrapper.find(RandomComponent)).toHaveLength(1)

    })
})