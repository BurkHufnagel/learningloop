import React from 'react';
import { shallow } from 'enzyme';
import ErrorBanner from '.'


describe('', () => {
    let wrapper, props
    beforeEach(() => {
        props = {
            message: 'Something Blew Up'
        }
        wrapper = shallow(<ErrorBanner  {...props} />);

    })
    it('should render with an element containing id', () => {
        expect(wrapper.find('#error-banner')).toHaveLength(1)
    })
    it('should render alert danger banner', () => {
        expect(wrapper.find('.error-banner.text-center'))
    })
    it('should render text passed in from the props', () => {
        expect(wrapper.find('#error-banner').text()).toEqual(props.message)

    })
})