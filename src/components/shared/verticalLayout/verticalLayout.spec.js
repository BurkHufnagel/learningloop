import React from 'react';
import { shallow } from 'enzyme';
import VerticalLayoutComponent from '.';


describe('', () => {
    let wrapper, RandomComponent = () => null


    it('renders without crashing', () => {
        wrapper = shallow(<VerticalLayoutComponent ><RandomComponent /></VerticalLayoutComponent>);

        expect(wrapper.find('.d-flex')).toHaveLength(1)
        expect(wrapper.find('.flex-column')).toHaveLength(1)
        expect(wrapper.find('.justify-content-around')).toHaveLength(1)
        expect(wrapper.find(RandomComponent)).toHaveLength(1)
    })

    it('should add class names for any extra classes passed in', () => {
        wrapper = shallow(<VerticalLayoutComponent classes={"somethingElse"}><RandomComponent /></VerticalLayoutComponent>);

        expect(wrapper.find('.d-flex')).toHaveLength(1)
        expect(wrapper.find('.flex-column')).toHaveLength(1)
        expect(wrapper.find('.justify-content-around')).toHaveLength(1)
        expect(wrapper.find('.somethingElse')).toHaveLength(1)
        expect(wrapper.find(RandomComponent)).toHaveLength(1)
    })


})