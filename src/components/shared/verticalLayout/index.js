import React from 'react'

class VerticalLayoutComponent extends React.Component {
    render() {
        return (<div className={`d-flex flex-column justify-content-around ${this.props.classes}`}>
            {this.props.children}
        </div>
        )
    }
}

export default VerticalLayoutComponent