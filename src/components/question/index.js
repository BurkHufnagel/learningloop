import React from 'react'
import CreateQuestion from './create'
import ShareQuestion from './share'
import { createQuestion } from '../../services/questions'

class Question extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            shareScreen: false,
            question: null
        }
    }

    createQuestion = async newQuestion => {
        let question = await createQuestion(newQuestion)
        this.setState({ shareScreen: true, question })
    }
    render() {
        return this.state.shareScreen ? <ShareQuestion question={this.state.question} /> : <CreateQuestion createQuestion={this.createQuestion} />
    }
}
export default Question