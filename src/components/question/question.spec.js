import React from 'react';
import { shallow } from 'enzyme';
import Question from '.'
import CreateQuestion from './create'
import ShareQuestion from './share'
import fetch from 'isomorphic-fetch'


describe('when a poll is being created and shared', () => {
    let wrapper
    beforeEach(() => {
        fetch.mockResolvedValue({ json: () => ({ question: 'something', id: '123saf' }) })
        wrapper = shallow(<Question />);

    })
    it('should start with a render create question component', () => {
        expect(wrapper.find(CreateQuestion)).toHaveLength(1)
        expect(wrapper.find(CreateQuestion).props().createQuestion).toBeTruthy()
        expect(wrapper.find(ShareQuestion)).toHaveLength(0)
    })

    it('should start with state for sharescreen as false', () => {
        expect(wrapper.state().shareScreen).toBe(false)
    })

    it('should start with state of question as null', () => {
        expect(wrapper.state().question).toBeNull()
    })

    it('should render ShareQuestion component after createQuestion is called', async () => {
        expect(wrapper.find(ShareQuestion)).toHaveLength(0)

        await wrapper.instance().createQuestion({ question: 'something' })
        expect(wrapper.find(ShareQuestion)).toHaveLength(1)
        expect(wrapper.find(ShareQuestion).props().question).toEqual({ question: 'something', id: '123saf' })
        expect(wrapper.find(CreateQuestion)).toHaveLength(0)
    })
})