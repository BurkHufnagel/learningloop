import React from 'react';
import { shallow } from 'enzyme';
import CreateQuestion from '.'



describe('', () => {
    let wrapper, props
    beforeEach(() => {
        props = {
            createQuestion: jest.fn().mockResolvedValue({ id: 'random' })
        }
        wrapper = shallow(<CreateQuestion {...props} />);

    })
    describe('Should be able to create a new poll', () => {
        it('should render with three elements a title, text area and button', () => {
            expect(wrapper.find('#poll-creator')).toHaveLength(1)
            expect(wrapper.find('h3#poll-creation-title')).toHaveLength(1)
            expect(wrapper.find('textarea#poll-question')).toHaveLength(1)
            expect(wrapper.find('button#submit-poll-btn')).toHaveLength(1)
        })

        it('should have classs for the textarea', () => {
            expect(wrapper.find('textarea#poll-question').props().className).toEqual("border border-primary container-fluid")
        })

        it('should have a set row length for the text area', () => {
            expect(wrapper.find('textarea#poll-question').props().rows).toEqual("5")

        })

        it('should have limit of 255 characters in the text area', () => {
            expect(wrapper.find('textarea#poll-question').props().maxLength).toEqual("255")
        })

        it('should have no default value in the text area', () => {
            expect(wrapper.find('textarea#poll-question').text()).toEqual('')
        })

        it('should update the state for the topic when you enter text', () => {
            wrapper.find('#poll-question').simulate('change', { target: { value: 'something new' }, preventDefault: jest.fn() })
            expect(wrapper.state().text).toEqual('something new')
        })

        it('should switch to the share screen after create btn clicked  ', async () => {
            wrapper.setState({ text: 'something to talk about' })
            wrapper.find('#submit-poll-btn').simulate('click')

            expect(wrapper.instance().props.createQuestion).toHaveBeenCalled()
        })
    })
})