import puppeteer from 'puppeteer'

const appURLBase = 'http://localhost:3000'

let browser;
let page;

beforeAll(async () => {
    browser = await puppeteer.launch()
    page = await browser.newPage()
})

describe('Learning Loop Specs', () => {
    describe('GIVEN the voter is on the voting screen ', () => {
        describe('AND has NOT voted yet', () => {
            describe('WHEN you the voter selects one of the 6 options (0,1,2,3,4,5)', () => {
                it('Then the voters selection is captured as their vote', async () => {

                    await page.goto(`${appURLBase}/11232`)
                    let App = await page.$('.App')
                    expect(App).not.toBeNull()

                    let buttonList = await page.$$('.fof-button')
                    expect(buttonList).toHaveLength(6)
                    await page.click('#vote_5')
                })
            })
        })
    })
    describe('GIVEN a user has navigated to the main menu ', () => {
        describe('WHEN they load the screen', () => {
            it('THEN they should see 3 links', async () => {

                await page.goto(`${appURLBase}`)
                let App = await page.$('.App')
                expect(App).not.toBeNull()

                let buttonList = await page.$$('a')
                expect(buttonList).toHaveLength(3)
            })
        })
        describe('WHEN a user clicks on the Create Fist To Five Poll', () => {
            it('THEN user taking to polls url to create new poll', async () => {
                await page.goto(`${appURLBase}`)
                await page.click("#create-poll-btn")

                expect(page.url()).toEqual(`${appURLBase}/polls`)
                expect((await page.$('#poll-creation-title'))).toBeTruthy()
                expect((await page.$('textarea'))).toBeTruthy()
                expect((await page.$('#submit-poll-btn'))).toBeTruthy()

                await page.type('textarea', "Something To type")

                await page.click('#submit-poll-btn')

                await page.waitForSelector('#share-poll')
                expect((await page.$('#share-poll'))).toBeTruthy()

                let urlHeader = await page.$('#poll-url-link')

                const text = await page.evaluate(urlHeader => urlHeader.textContent, urlHeader)
                await page.click('#go-to-poll-btn')

                expect(page.url().split('/').pop()).toEqual(text.split('/').pop())
            })
        })

        describe('WHEN a user clicks on Info Btn', () => {
            it('THEN user taking to ino url to learn about fist of five', async () => {
                await page.goto(`${appURLBase}`)
                await page.click("#info-btn")
                expect(page.url()).toEqual(`${appURLBase}/info`)
            })
        })

        describe('WHEN a user clicks on Info Btn', () => {
            it('THEN user taking to ino url to learn about fist of five', async () => {
                await page.goto(`${appURLBase}`)
                await page.click("#join-btn")
                expect(page.url()).toEqual(`https://gitlab.com/learning-loop`)
            })
        })

    })
})
afterAll(async () => {
    await browser.close()
})
